﻿#ifndef WIDGETVERIFY_H
#define WIDGETVERIFY_H

#include <QWidget>
#include "widgetverify_global.h"

#include "widget1.h"

class WIDGETVERIFYSHARED_EXPORT WidgetVerify:public QWidget
{

public:
    WidgetVerify(QWidget* parent = 0);
    ~WidgetVerify();

    widget1* subWidget;
};

#endif // WIDGETVERIFY_H
