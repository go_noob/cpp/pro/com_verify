﻿#include "widgetverify.h"


WidgetVerify::WidgetVerify(QWidget *parent)
    :QWidget(parent)
{
    this->setWindowTitle("Hello World!");
    subWidget = new widget1(this);

    subWidget->setWindowFlags(Qt::Window);
    subWidget->show();
}
WidgetVerify::~WidgetVerify()
{
    if(nullptr != subWidget)
    {
        delete subWidget;
        subWidget = nullptr;
    }
}
