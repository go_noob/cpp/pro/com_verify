# COM接口实现形式验证

## 环境
系统：windows10  
编辑器：Visual studio 2017、Qt Creator 4.4.1  
Qt版本：5.9.3

## 使用方法

本项目由5部分组成，其中 4 个VS项目(2个Dll项目，一个控制台项目，一个QtGUI项目)存放在Basic文件夹下，1个QtCreator项目(DLL项目)存放在QtRegist文件夹下，使用方法：
1. 下载代码；
2. 通过QtCreator可以打开QtRegist下的WidgetVerify这一Qt项目，编译生成Dll文件和lib文件
3. 通过VS2017来打开Basic文件下的解决方案，打开之后会看到此解决方案下有4个项目，需要依次生成，顺序：
   1. 构建Basic，生成Basic.dll,Basic.lib
   2. 配置Regist项目，需要将Basic项目的.h文件路径，lib文件路径逐一进行配置，之后生成，生成Basic.dll,Basic.lib，需要注意的是，如果Basic.dll不在本项目的目标文件生成位置
   3. 配置Use项目，需要将Basic项目的.h文件路径，lib文件路径逐一进行配置，这里需要注意的是，如果Regist.dll文件不在Use.exe的同级目录，需要对main.cpp的第10行即`std::string path = "Regist.dll";`这句代码的文件路径进行修改，以确保程序可以找到动态库的位置，配置完成后可构建运行，结果如图： 
   ![result](./images/Use项目结果.PNG)
   4. 配置Widow项目(这诡异的名字是因为创建项目的时候少写了个n。。。应该是window)，这里需要配置步骤2. WidgetVerify项目生成的WidgetVerify.dll,WidgetVerify.lib，以及相关头文件路径，配置完成后可构建运行，运行结果如图：
	![result](./images/Widow项目结果.PNG)

## 各项目内容说明
在这里对如何通过接口来实现COM(component object model)框架来进行验证，不同文件夹的作用说明如下：  
1. Basic：这里完成最近本的框架实现，以及最终测试，在这下面分为以下几个文件夹：  
	1. Basic文件夹(dll项目)：  
		依赖项：无  
		实现内容：基础框架实现，包括：数据接口(Database.h)、全局注册管理(RegistBase.h)以及一个导出声明(Basic.h，这是创建项目时自动生成的文件，其实手写也可以)  
		作用：为其它项目提供：.h, .dll, .lib文件
	2. Regist文件夹(dll项目):   
		依赖项：Basic项目生成的 .h,.dll,.lib文件  
		实现内容：注册验证，在这里实现一个自定义类，并通过注册宏将其注册到全局管理当中(注册动作在加载dll时完成)  
		作用：为其它项目提供 .dll 文件  
	3. Use文件夹(控制台项目)：  
		依赖项：1. Basic项目生成的 .h,.dll,.lib文件； 2. Regist项目的 .dll 文件  
		内容：用来对项目进行验证  
		作用：输出验证  
	4. Window文件夹(Qt GUI 项目)：  
		依赖项：1. 依赖于QtRegist文件夹中WidgetVerify项目生成的 .h,.dll,.lib文件 	
		内容：因为全局注册可行性已通过1.1~1.3进行了验证，所以这里只对窗口通过dll文件封装与传递进行验证，由于需要验证Qt的相关组件能否通过dll传递，所以先对通过QtCreator创建的dll文件进行验证  
		作用：Qt窗口验证  

2. QtRegist：这里存放通过QtCreator创建的项目进行存放  
	2.1 WidgetVerify(dll项目)：  
		依赖项：Qt  
		内容：尝试将Qt的widget封装到dll文件当中，之后由其他程序调用这一dll文件，并创建其中的窗口  
		作用：通过QtCreator生成窗口验证所需的dll文件  
