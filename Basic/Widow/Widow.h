#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_Widow.h"

#include <Windows.h>
#include "widgetverify.h"
class Widow : public QMainWindow
{
	Q_OBJECT

public:
	Widow(QWidget *parent = Q_NULLPTR);
	~Widow();

private:
	Ui::WidowClass ui;

	WidgetVerify* testWidget;
	QWidget* verifyPtr;
	HMODULE hDllLib;
};
