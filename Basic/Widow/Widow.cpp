#include "Widow.h"

Widow::Widow(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	testWidget = new WidgetVerify(this);
	testWidget->setWindowFlags(Qt::Window);
	testWidget->show();
}
Widow::~Widow()
{
	if (nullptr != testWidget)
	{
		delete testWidget;
		testWidget = nullptr;
	}
}
