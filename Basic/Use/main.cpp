#include <iostream>
#include "RegistBase.h"
#include <Windows.h>
#include <vector>

#define TEST_TITLE(content) "=============== " ## #content ## " ==============="
int main(int argc, char** argv)
{
	//-------------------dll 加载-----------
	std::string path = "Regist.dll"; // 指定dll库位置及名称
	HMODULE hDllLib = LoadLibrary(path.c_str());

	//--------------basic test-------------
	/*
	测试目的：
			1.验证 Regist 项目中 Teset 类，在构造函数当中会向 Basic 项目中的 RegistBase 类中的 m_list 当中写入若干 int 型数据
	*/
	std::cout << TEST_TITLE(BasicTest) << std::endl;
	int count = RegistBase::GetList().size();
	int value = RegistBase::GetList().at(0);
	//std::vector<int> list = RegistBase::GetList();

	std::cout << "size" <<count << std::endl;
	std::vector<int>::iterator iter = RegistBase::GetList().begin();
	int i = 0;
	for (; iter != RegistBase::GetList().end(); ++iter)
	{
		std::cout << i << " : " << (*iter) << std::endl;
		i++;
	}

	//--------------class test-------------
	/*
	测试目的：
			1.验证 Regist 项目中 TesetRegist 类，这一类从 Basic 项目中的 DataBase 类派生
			2.验证 Regist 项目的 注册宏， 注册之后的内容记录在 Basic 项目中的 RegistBase 类中的 m_dataList 当中
	*/
	std::cout << TEST_TITLE(VirtualClassTest) << std::endl;
	
	count = RegistBase::GetDataList().size();
	DataBase* ptr = RegistBase::GetDataList().at(0);
	value = ptr->GetValue();
	std::string str = ptr->GetString("Hello world! ");

	std::cout <<"class name: "<<ptr->GetName()<< "\nvalue: " << value << "\nstring: " << str << std::endl;

	//--------------free dll------------
	FreeLibrary(hDllLib);
	return 0;
}