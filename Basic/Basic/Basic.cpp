//// Basic.cpp : 定义 DLL 应用程序的导出函数。
////
//
//#include "Basic.h"
//
//
//// 这是导出变量的一个示例
//BASIC_API int nBasic=0;
//
//// 这是导出函数的一个示例。
//BASIC_API int fnBasic(void)
//{
//    return 42;
//}
//
//// 这是已导出类的构造函数。
//CBasic::CBasic()
//{
//    return;
//}
