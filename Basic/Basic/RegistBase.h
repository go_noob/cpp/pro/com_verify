#pragma once

#include "Basic.h"
#include <vector>
#include "DataBase.h"
//template class BASIC_API std::allocator<int>;
//template struct BASIC_API std::_Vec_base_types<int, std::allocator<int>>;
//template class BASIC_API std::_Vector_alloc<std::_Vec_base_types<int, std::allocator<int>>>;
//template class BASIC_API std::_Compressed_pair<std::allocator<int>, std::_Vector_val<std::_Simple_types<_Ty>>, true>;
//template class BASIC_API std::vector<int, std::allocator<int>> ;

class BASIC_API RegistBase
{
public:
	RegistBase();
	virtual ~RegistBase();

	static std::vector<int>& GetList();
	static std::vector<DataBase*>& GetDataList();

	static std::vector<int>* m_list;
	static std::vector<DataBase*>* m_dataList;

};

static RegistBase globalRegistBase;

