#pragma once

#include"Basic.h"

#include <string>

class BASIC_API DataBase
{
public:
	DataBase() {}
	virtual ~DataBase() {}

	virtual int GetValue() = 0;
	virtual std::string GetName() = 0;
	virtual std::string GetString(const std::string& str) {
		return str + "_success";
	}
};