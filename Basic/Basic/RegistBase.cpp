#include "RegistBase.h"

std::vector<int>* RegistBase::m_list = nullptr;
std::vector<DataBase*>* RegistBase::m_dataList = nullptr;
RegistBase::RegistBase()
{
	if (nullptr == m_list)
	{
		m_list = new std::vector<int>();
	}
	if (nullptr == m_dataList)
	{
		m_dataList = new std::vector<DataBase*>();
	}
}


RegistBase::~RegistBase()
{
	if (nullptr != m_list)
	{
		delete m_list;
		m_list = nullptr;
	}
}
std::vector<int>& RegistBase::GetList(){ 
	return *m_list; 
}
std::vector<DataBase*>& RegistBase::GetDataList() {
	return *m_dataList;
}
