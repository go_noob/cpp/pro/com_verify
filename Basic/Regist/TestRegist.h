#pragma once
#include "Global.h"
#include "RegistBase.h"
#include "DataBase.h"
class REGIST_API Test
{
public:
	Test():m_value(25) {
		RegistBase::m_list->push_back(m_value);
		RegistBase::m_list->push_back(m_value+1);
		RegistBase::m_list->push_back(m_value+2);

	}
	~Test(){}
	int GetNum() { return 25; }

	int m_value;
};

static Test globalTest;

class REGIST_API TestData :public DataBase
{
public:
	TestData() {}
	virtual ~TestData() {}

	virtual int GetValue() { return 25555; }
	virtual std::string GetName() { return "TestData"; }
};

#define REGIST_CLASS(name) class name ## Hide \
{\
public:\
name##Hide(){\
RegistBase::m_dataList->push_back(new name());\
}\
};\
static name##Hide global ## name ## Hide;


REGIST_CLASS(TestData);
